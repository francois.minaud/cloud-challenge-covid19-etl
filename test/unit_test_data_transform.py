import requests
from io import BytesIO
from process_covid19_data_module import transform_data

url_nyt_original = "https://raw.githubusercontent.com/nytimes/covid-19-data/master/us.csv"
url_nyt_wrong_header = "https://raw.githubusercontent.com/francoisminaud/CloudGuruChallenge_September2020/master/nyt_us_wrong_header.csv"
url_nyt_wrong_data = "https://raw.githubusercontent.com/francoisminaud/CloudGuruChallenge_September2020/master/nyt_us_string_in_deaths_count.csv"

url_hopkins_original = "https://raw.githubusercontent.com/datasets/covid-19/master/data/time-series-19-covid-combined.csv"
url_hopkins_wrong_data = "https://raw.githubusercontent.com/francoisminaud/CloudGuruChallenge_September2020/master/hopkins_string_in_deaths_count.csvhopkins_string_in_recovered_count.csv"
url_hopkins_wrong_header = "https://raw.githubusercontent.com/francoisminaud/CloudGuruChallenge_September2020/master/hopkins_wrong_header.csv"


def unit_test(url_nyt, url_hopkins):

    #we stub the first part of the basic module and want to unit test out transform_data() function

    response_nyt = requests.get(url_nyt, stream=True,allow_redirects=True)
    nyt_csv_dump = BytesIO(response_nyt.content)
    response_hopkins = requests.get(url_hopkins, stream=True,allow_redirects=True)
    hopkins_csv_dump = BytesIO(response_hopkins.content)

    message, my_dict = transform_data(nyt_csv_dump,hopkins_csv_dump)
    return message, my_dict



#Make sure we got OK message when all is fine
message, my_dict = unit_test(url_nyt_original,url_hopkins_original)
assert( message == "")


#Make sure we get errors when introducing errors in NYT data
message, my_dict = unit_test(url_nyt_wrong_header,url_hopkins_original)
assert(message ==  "Malformed data in NYT CSV file (header data invalid), abort operation")
message, my_dict = unit_test(url_nyt_wrong_data,url_hopkins_original)
assert(message ==  "Malformed data in NYT CSV file (Cases or Deaths data invalid), abort operation")

#Make sure we get errors when introducing errors in Hopkins data
message, my_dict = unit_test(url_nyt_original,url_hopkins_wrong_header)
assert(message ==  "Malformed data in Hopkins CSV file (header data invalid), abort operation")
message, my_dict = unit_test(url_nyt_original,url_hopkins_wrong_data)
assert(message ==  "Malformed data in Hopkins CSV file (Recovered data invalid), abort operation")