variable "DB_USERNAME" {
    type = "string"
}

variable "DB_PASSWORD" {
    type = "string"
}

provider "aws" {
   profile    = "default"
   region     = "eu-west-3"
 }


# Cannot create terraform state file in this project, so it should be created manually
# aws s3api create-bucket --bucket covid19-etl-terraform-state --region eu-west-3 --create-bucket-configuration LocationConstraint=eu-west-3


terraform {
  backend "s3" {
    bucket         = "covid19-etl-terraform-state-production"
    key            = "terraform.tfstate"
    region         = "eu-west-3"
    encrypt        = true
  }
}

// aws s3 cp ./function.zip  s3://covid19-etl-lambda

/*resource "aws_s3_bucket" "s3-lambda-production" {
  bucket = "s3-lambda-production"
  acl    = "private"
  force_destroy = "true"

  tags = {
    Name        = "s3-lambda-production"
    Environment = "Dev"
  }
}*/

resource "aws_s3_bucket" "s3-etl-fail-production" {
  bucket = "s3-etl-fail-production"
  acl    = "private"
  force_destroy = "true"

  tags = {
    Name        = "s3-etl-fail-production"
    Environment = "Dev"
  }
}



resource "aws_lambda_function" "lambda_production" {
  s3_bucket     = "s3-lambda-production"
  s3_key        = "covid19_etl_lambda_function.zip"
  function_name = "lambda_covid19etl_production"
  handler       = "process_covid19_us_stats.lambda_handler"
  role          = "${aws_iam_role.lambda_role_production.arn}"
  runtime       = "python3.6"
  timeout       = 300
}


#create my RDS PostgresSQL database
resource "aws_db_instance" "covid19_db_production" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "postgres"
  engine_version       = "12.3"
  instance_class       = "db.t2.micro"
  identifier           = "database-covid19etl-production"
  name                 = "devfrancois"
  username             = "${var.DB_USERNAME}"
  password             = "${var.DB_PASSWORD}"
  parameter_group_name = "default.postgres12"
  publicly_accessible  = true
  port                 = 5432
  skip_final_snapshot  = true
}

#creation of my SNS topic

resource "aws_sns_topic" "covid19_database_update_production" {
    name = "covid19_database_update_production"
}

resource "aws_iam_policy" "lambda_policy_production" {
  name = "LambdaPolicy_production"
  path = "/"

  policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Effect": "Allow",
			"Action": [
				"logs:CreateLogStream",
				"logs:PutLogEvents"
			],
			"Resource": "arn:aws:logs:eu-west-3:888038558695:*"
		},
		{
			"Effect": "Allow",
			"Action": "logs:CreateLogGroup",
			"Resource": "*"
		},
		{
			"Sid": "SNSPublishSubscribe",
			"Effect": "Allow",
    		"Action": [
                      "sns:Publish",
                      "SNS:Subscribe"
            ],
    		"Resource": "arn:aws:sns:eu-west-3:888038558695:covid19_database_update_production"
  		},
		{
			"Sid": "SNSGetList",
			"Effect": "Allow",
		"Action": [
                  "sns:ListTopics",
                  "SNS:ListSubscriptionsByTopic"
            ],
    		"Resource": "arn:aws:sns:eu-west-3:888038558695:*"
  		},
       {
            "Sid": "AllowRDSOperations",
            "Effect": "Allow",
            "Action": [
                "rds:DescribeDBInstances",
                "rds:DescribeDBClusterSnapshots"
            ],
            "Resource": "*"
        },
       {
            "Sid": "AllowSSNOperations",
            "Effect": "Allow",
            "Action": [
                "ssm:GetParameter"
            ],
            "Resource": [
              "arn:aws:ssm:eu-west-3:888038558695:parameter/Covid19_Postgres_DB_secrets_login",
              "arn:aws:ssm:eu-west-3:888038558695:parameter/Covid19_Postgres_DB_secrets_password"
            ]
        },
       {
            "Sid": "AllowS3PutGet",
            "Effect": "Allow",
            "Action": [
                 "s3:ListBucket",
                 "s3:GetObject",
                 "s3:PutObject",
                 "s3:HeadObject"
            ],
            "Resource": "arn:aws:s3:::s3-etl-fail-production/*"
        }
	]
}
EOF
}

resource "aws_iam_role" "lambda_role_production" {
  name = "LambdaCovid19Role_production"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "lambda.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        },
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "sns.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        },
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "rds.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        },
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "s3.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "Lambda_postgres_sns_attachment_production" {
  role       = "${aws_iam_role.lambda_role_production.name}"
  policy_arn = "${aws_iam_policy.lambda_policy_production.arn}"
}


resource "aws_cloudwatch_event_rule" "every-day-10am-utc-production" {
  name                = "every-day-10am-utc"
  description         = "Fires_every_day_at_10am_UTC"
  schedule_expression = "cron(0 12 * * ? *)"
}

resource "aws_cloudwatch_event_target" "fire-event-every-day-10am-production" {
  rule      = "${aws_cloudwatch_event_rule.every-day-10am-utc-production.name}"
  target_id = "lambda_function_10am_UTC_every_day"
  arn       = "${aws_lambda_function.lambda_production.arn}"
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_lambda_production" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda_production.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.every-day-10am-utc-production.arn}"
}
