#!/bin/sh

#As we cannot create those using terraform, let's do this manually
aws s3api create-bucket --bucket covid19-etl-terraform-state-production --region eu-west-3 --create-bucket-configuration LocationConstraint=eu-west-3
aws s3api create-bucket --bucket covid19-etl-terraform-state-smoketest --region eu-west-3 --create-bucket-configuration LocationConstraint=eu-west-3
aws s3api create-bucket --bucket s3-lambda-production --region eu-west-3 --create-bucket-configuration LocationConstraint=eu-west-3
aws s3api create-bucket --bucket s3-lambda-smoke --region eu-west-3 --create-bucket-configuration LocationConstraint=eu-west-3
