import pandas as pd

#import time
#start_module_time = time.time()

#TODO rename parameters
def transform_data(x,y):

    date_nyt = []
    cases_nyt = []
    deaths_nyt = []

    date_hopkins = []
    recoveries_hopkins = []

    joint_list_date = []
    joint_list_cases = []
    joint_list_deaths = []
    joint_list_recoveries = []

    my_dict = {}
    message = ""

    #let's check for invalid data first
    df_nyt = pd.read_csv(x)

    if not set(['date', 'cases','deaths']).issubset(df_nyt.columns):
        message = "Malformed data in NYT CSV file (header data invalid), abort operation"
        return message, my_dict


    try:
        for i, j in df_nyt.iterrows():
            date_nyt.append(pd.to_datetime(j['date'], errors='coerce', infer_datetime_format=True, format='%Y-%m-%d').date())
            cases_nyt.append(j['cases'])
            deaths_nyt.append(j['deaths'])
            if not type(j['cases']) == int or not type(j['deaths']) == int:
                message = "Malformed data in NYT CSV file (Cases or Deaths data invalid), abort operation"
                return message, my_dict
    except:
        #print("Parsing error for NYT data, exiting")
        raise

    #Parse Hopkins CSV File but first let's check for invalid data

    df_hop = pd.read_csv(y)

    if not set(['Country/Region', 'Date','Recovered']).issubset(df_hop.columns):
        message = "Malformed data in Hopkins CSV file (header data invalid), abort operation"
        return message, my_dict

    try:
        for i, j in df_hop.iterrows():
            if j['Country/Region'] == "US":
                hopkins_date_lookup_for_nyt_list = pd.to_datetime(j['Date'], errors='coerce', infer_datetime_format=True, format='%Y-%m-%d').date()
                for idx, date in enumerate(date_nyt):
                    if date == hopkins_date_lookup_for_nyt_list:
                        date_hopkins.append(hopkins_date_lookup_for_nyt_list)
                        recoveries_hopkins.append(j['Recovered'])
                        #somehow pandas read the Recovered data as float, let's adapt!!
                        if not type(j['Recovered'])  == float:
                            message = "Malformed data in Hopkins CSV file (Recovered data invalid), abort operation"
                            return message,my_dict
    except:
        #print("Parsing error for Hopkins data, exiting")
        raise


    #Let's join the NYT and Hopkins tables
    for idx_nyt, loop_date_nyt in enumerate(date_nyt):
        for idx_hop, loop_date_hop in enumerate(date_hopkins):
            if loop_date_hop == loop_date_nyt:
                # get date, cases, deaths from NYT, recoveries frop Hopkins
                joint_list_date.append(loop_date_hop)
                joint_list_cases.append(cases_nyt[idx_nyt])
                joint_list_deaths.append(deaths_nyt[idx_nyt])
                joint_list_recoveries.append(recoveries_hopkins[idx_hop])

    my_dict = {'Date': joint_list_date, 'Cases': joint_list_cases, 'Deaths': joint_list_deaths, 'Recoveries': joint_list_recoveries }

    #print("--- %s seconds ---" % (time.time() - start_module_time))

    #return tuple
    return message, my_dict
