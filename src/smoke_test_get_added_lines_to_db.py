import sys
import psycopg2
import boto3

# define a function that handles and parses psycopg2 exceptions
# used for dev purpose
def print_psycopg2_exception(err):
    # get details about the exception
    err_type, err_obj, traceback = sys.exc_info()

    # get the line number when exception occured
    line_num = traceback.tb_lineno
    # print the connect() error
    print("\npsycopg2 ERROR:", err, "on line number:", line_num)
    print("psycopg2 traceback:", traceback, "-- type:", err_type)

    # psycopg2 extensions.Diagnostics object attribute
    print("\nextensions.Diagnostics:", err.diag)

    # print the pgcode and pgerror exceptions
    print("pgerror:", err.pgerror)
    print("pgcode:", err.pgcode, "\n")

def connect_database():

    #this is the default database name, let's not change it
    database = "postgres"

    client = boto3.client('rds')
    #TODO: hide it in SSM?
    db_instance = 'database-covid19etl-smoke'
    response = client.describe_db_instances(DBInstanceIdentifier=db_instance)
    rds_host = response.get('DBInstances')[0].get('Endpoint').get('Address')
    hostname = rds_host

    ssm = boto3.client('ssm')
    parameter_login = ssm.get_parameter(Name='Covid19_Postgres_DB_secrets_login', WithDecryption=False)
    username = parameter_login['Parameter']['Value']
    parameter_pw = ssm.get_parameter(Name='Covid19_Postgres_DB_secrets_password', WithDecryption=False)
    password = parameter_pw['Parameter']['Value']
    port = '5432'
    myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)

    return myConnection


myConnection = connect_database()
cursor = myConnection.cursor()

cursor.execute("SELECT id, Date,Cases,Deaths,Recoveries FROM covid19 ORDER BY Date DESC LIMIT 1")
myConnection.commit()
last_record = cursor.fetchone()

#print(last_record)

#write last_record to file so it can be compared later on
text_file = open("smoke_test_get_added_lines_to_db.txt", "w")
text_file.write(str(last_record[0]))
text_file.close()

