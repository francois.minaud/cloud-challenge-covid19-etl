from io import BytesIO
import requests
from process_covid19_data_module import transform_data
import botocore
import pickle
#For debugging and performance purpose
#import time
#start_time = time.time()

date_nyt = []
cases_nyt = []
deaths_nyt = []
recoveries_nyt = []

date_hopkins = []
recoveries_hopkins = []

joint_list_date = []
joint_list_cases = []
joint_list_deaths = []
joint_list_recoveries = []

fail_index = []
fail_date = []
fail_cases = []
fail_deaths = []
fail_recoveries = []


country = ""

nyt_data = []
hopkins_data = []

import sys

import psycopg2
import boto3

# define a function that handles and parses psycopg2 exceptions
# used for dev purpose
def print_psycopg2_exception(err):
    # get details about the exception
    err_type, err_obj, traceback = sys.exc_info()

    # get the line number when exception occured
    line_num = traceback.tb_lineno
    # print the connect() error
    print("\npsycopg2 ERROR:", err, "on line number:", line_num)
    print("psycopg2 traceback:", traceback, "-- type:", err_type)

    # psycopg2 extensions.Diagnostics object attribute
    print("\nextensions.Diagnostics:", err.diag)

    # print the pgcode and pgerror exceptions
    print("pgerror:", err.pgerror)
    print("pgcode:", err.pgcode, "\n")


def connect_database():

    #this is the default database name, let's not change it
    database = "postgres"

    client = boto3.client('rds')
    #TODO: hide it in SSM?
    db_instance = 'database-covid19etl-production'
    response = client.describe_db_instances(DBInstanceIdentifier=db_instance)
    rds_host = response.get('DBInstances')[0].get('Endpoint').get('Address')
    hostname = rds_host

    ssm = boto3.client('ssm')
    parameter_login = ssm.get_parameter(Name='Covid19_Postgres_DB_secrets_login', WithDecryption=False)
    username = parameter_login['Parameter']['Value']
    parameter_pw = ssm.get_parameter(Name='Covid19_Postgres_DB_secrets_password', WithDecryption=False)
    password = parameter_pw['Parameter']['Value']
    port = '5432'
    myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)

    return myConnection


def insert_data_to_db(myConnection, cursor, my_dict, starting_index, retry):
    count_added_rows = 0
    # let's insert all the rows of the dictionary or some, depending on starting_index
    if (starting_index in range(-len(my_dict["Date"]), len(my_dict["Date"]))) == False:
        return 0

    # we want to iterate though the list of a dictionary
    for i in range(starting_index, len(my_dict["Date"])):
        sql_update_query = "INSERT INTO covid19(id, Date,Cases,Deaths,Recoveries) VALUES(%s,%s,%s,%s,%s) ON CONFLICT (id) DO NOTHING"
        try:
            cursor.execute(sql_update_query, ( i, my_dict["Date"][i], my_dict["Cases"][i], my_dict["Deaths"][i], my_dict["Recoveries"][i]))
            myConnection.commit()
        except Exception as err:
            print_psycopg2_exception(err)
            myConnection.rollback()
            #prepare new dict
            if retry == False:
                for save in range(i, len(my_dict["Date"])):
                    fail_index.append(i)
                    fail_date.append(my_dict["Date"][i])
                    fail_cases.append(my_dict["Cases"][i])
                    fail_deaths.append(my_dict["Deaths"][i])
                    fail_recoveries.append(my_dict["Recoveries"][i])
                #print("Save to the new dictionary")
                fail_dict = {'Index' : fail_index,'Date': fail_date, 'Cases': fail_cases, 'Deaths': fail_deaths, 'Recoveries': fail_recoveries }
                serializedData = pickle.dumps(fail_dict)
                s3 = boto3.client('s3')
                s3.put_object(Bucket='s3-etl-fail-production', Key='failed_session',  Body=serializedData)
            #eventually still return count_added_rows
            return count_added_rows

        #make sure that we have added a row here
        row_cnt = cursor.rowcount
        if row_cnt == 1:
            count_added_rows += row_cnt

    #eventually return the function with the number of added rows
    return count_added_rows

def initial_database_load(myConnection, cursor, my_dict):
    starting_index = 0

    count_added_row = insert_data_to_db(myConnection,cursor,my_dict,starting_index, False)
    send_sns_message("Covid19 Database was just created and loaded with "+ str(count_added_row) +" rows")

def update_database_table(myConnection, cursor, my_dict, retry):

    #LOOK for ID of last inserted item
    try:
        cursor.execute("SELECT id, Date,Cases,Deaths,Recoveries FROM covid19 ORDER BY Date DESC LIMIT 1")
        myConnection.commit()
        last_record = cursor.fetchone()

    except Exception as err:
        print("problem occured during last item selection")
        print_psycopg2_exception(err)
        myConnection.rollback()

    #corner case: DB table was previously created but no data was inserted => cannot get last inserted row id
    if last_record == None:
        last_table_index = 0
    else:
        last_table_index = last_record[0]

    count_added_row = insert_data_to_db(myConnection, cursor, my_dict, last_table_index+1, retry)

    #freeing dictionary, lambda function may be in a warm state for some time after exiting, we want to avoid memory problems
    my_dict.clear()

    if count_added_row >= 1:
        send_sns_message("Covid19 Database was just updated with "+ str(count_added_row) +" new rows")
    else:
        send_sns_message("Covid19 Database is already up to date, no new data was inserted")


def send_sns_message(sns_message):
    import boto3
    # Create an SNS client
    sns = boto3.client('sns','eu-west-3')

    session = boto3.session.Session()
    client = session.client('sns', region_name='eu-west-3')
    arn = client.list_topics()

    found_subscription = False
    #Get our SNS ARN from boto3 so we don't hardcode the ARN or the AWS ID
    for arn in client.list_topics().get('Topics'):
        if arn['TopicArn'].rsplit(':',1)[1] == "covid19_database_update_production":
            topic_arn = arn['TopicArn']
            dict_sub = client.list_subscriptions_by_topic(TopicArn=topic_arn)
            for subs in dict_sub['Subscriptions']:
                if subs['Endpoint'] == "francois.minaud@gmail.com":
                    found_subscription = True
            #Only subscribe to the topic if given user is not subscribed yet
            if not found_subscription:
                client.subscribe(TopicArn=topic_arn, Protocol="email", Endpoint="francois.minaud@gmail.com")
            #let's break the for loop now that our subscription is done
            break



    # Publish a simple message to the specified SNS topic
    response = sns.publish(
        TopicArn = (topic_arn),
        Message = (sns_message),
    )

def failed_session_pending():

    mys3 = boto3.client('s3')
    try:
        mys3.head_object(Bucket='s3-etl-fail-production',Key='failed_session')
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            #print("S3 bucket not found, proceed with normal processing")
            return False
        else:
            return False
    return True

def lambda_handler(event, context):

    #first let's check if we have a pending file we need to save
    if failed_session_pending():
        print("let try to save our s3 bucket content!!!")
        #now retry the save data to db
        myConnection = connect_database()
        cursor = myConnection.cursor()
        s3 = boto3.client('s3')
        myObject = s3.get_object(Bucket='s3-etl-fail-production',Key='failed_session')
        serializedObject = myObject['Body'].read()
        my_s3_dict = pickle.loads(serializedObject)
        update_database_table(myConnection, cursor, my_s3_dict, True)

    #here we dump the CSV files in memory
    url_nyt = "https://raw.githubusercontent.com/nytimes/covid-19-data/master/us.csv"
    url_hopkins = "https://raw.githubusercontent.com/datasets/covid-19/master/data/time-series-19-covid-combined.csv"
    response_nyt = requests.get(url_nyt, stream=True,allow_redirects=True)
    nyt_csv_dump = BytesIO(response_nyt.content)
    response_hopkins = requests.get(url_hopkins, stream=True,allow_redirects=True)
    hopkins_csv_dump = BytesIO(response_hopkins.content)


    message, my_dict = transform_data(nyt_csv_dump, hopkins_csv_dump)
    if not message == "":
        return message

    #connect to database
    myConnection = connect_database()
    cursor = myConnection.cursor()

    #create db if doesn't exist
    try:
        cursor.execute("CREATE TABLE covid19(id SERIAL PRIMARY KEY, Date DATE, Cases INT, Deaths INT, Recoveries INT)")
        myConnection.commit()
        #now do initial load
        initial_database_load(myConnection, cursor, my_dict, False)
    except:
        myConnection.rollback()
        update_database_table(myConnection, cursor, my_dict, False)

    #print("Lambda function over, exiting")
    return event

#mock event and context to test lambda function locally
lambda_handler("hello","you")

#for debugging and performance purpose
#print("--- %s seconds ---" % (time.time() - start_time))

